package com.mrren.wordreplace;

public class Test {
    public static void main(String[] args) {
        A a1 = new A();
        a1.setI(1);
        A a2 = new A();
        a1 = a2;
        a2.setI(2);
        System.out.println(a1.getI());

    }


}
class A{
    private int i;

    public int getI() {
        return i;
    }

    public void setI(int i) {
        this.i = i;
    }
}