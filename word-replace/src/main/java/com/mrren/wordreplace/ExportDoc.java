package com.mrren.wordreplace;

import freemarker.template.Configuration;
import freemarker.template.Template;
import sun.misc.BASE64Encoder;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

public class ExportDoc {
    
    private Configuration configuration;
    private String encoding;
    
    public ExportDoc(String encoding) {
        this.encoding = encoding;
        configuration = new Configuration(Configuration.VERSION_2_3_22);
        configuration.setDefaultEncoding(encoding);
        try {
            configuration.setDirectoryForTemplateLoading(new File("d:\\"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public Template getTemplate(String name) throws Exception {
        return configuration.getTemplate(name);
    }
    


    public void exportDoc(String doc, String name) throws Exception {
        Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(doc), encoding));
        getTemplate(name).process(readData(), writer);
    }
    private static Map<String, Object> readData(){
        Map<String, Object> result = new HashMap<>();

        File file = new File("d:\\data.txt");
        BufferedReader reader = null;
        try {
            System.out.println("以行为单位读取文件内容，一次读一行");
            InputStreamReader isr = new InputStreamReader(new FileInputStream(file), "UTF-8");
            reader = new BufferedReader(isr);
            String tempString = null;
            int line = 1;
            //一次读一行，读入null时文件结束
            while ((tempString = reader.readLine()) != null) {
                //把当前行号显示出来
                System.out.println("line " + line + ": " + tempString);
                String[] str = tempString.split("=");
                if (str.length == 2) {
                    result.put(str[0], str[1]);
                }
                line++;
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        }
        return result;
    }
    
}