package com.mrren.wordreplace;

import org.springframework.stereotype.Component;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

@Component
public  class ReplaceTool {

    public static void doReplace(){

        System.out.println("aaa");

        FreemarkerUtils.processTemplate(readTemplate(), readData());

    }

    private static Map<String, Object> readData(){
        Map<String, Object> result = new HashMap<>();

        File file = new File("d:/data.txt");
        BufferedReader reader = null;
        try {
            System.out.println("以行为单位读取文件内容，一次读一行");
            reader = new BufferedReader(new FileReader(file));
            String tempString = null;
            int line = 1;
            //一次读一行，读入null时文件结束
            while ((tempString = reader.readLine()) != null) {
            //把当前行号显示出来
                System.out.println("line " + line + ": " + tempString);
                String[] str = tempString.split("=");
                if (str.length == 2) {
                    result.put(str[0], str[1]);
                }
                line++;
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        }
        return result;
    }

    private static String readTemplate()
    {
        int len=0;
        StringBuffer str=new StringBuffer("");
        File file=new File("d:/template.ftl");
        try {
            FileInputStream is=new FileInputStream(file);
            InputStreamReader isr= new InputStreamReader(is);
            BufferedReader in= new BufferedReader(isr);
            String line=null;
            while( (line=in.readLine())!=null )
            {
                str.append(line);
                len++;
            }
            in.close();
            is.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return str.toString();
    }
}
